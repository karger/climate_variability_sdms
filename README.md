# climate_variability_sdms

Global species distributions for mammals, reptiles, and amphibians based on different climate predictor groups.

The code here models the distribution of 730 amphibian, 1276 reptile, and 1961 mammal species globally as a function of current climate at a 0.5° spatial resolution using four different predictor groups composed of different combinations of input variables: mean climatic conditions, spatial climatic variability, and temporal (interannual) climatic variability.

## Contributing
Karger, Dirk Nikolaus 
Saladin, Bianca 
Wüest, Rafael O. 

## Data availability
Karger, Dirk Nikolaus; Saladin, Bianca; Wüest, Rafael O.; Graham, Catherine H.; Zurell, Damaris; Mo, Lidong; Zimmermann, Niklaus E. (2022). Global species distributions for mammals, reptiles, and amphibians. EnviDat. doi:10.16904/envidat.354.

## License
CC0

## Project status
under review

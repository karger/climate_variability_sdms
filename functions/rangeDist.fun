rangeDist<-function (range, points, domain, domainkm = 1000, fact = 2, mask = FALSE, 
          verbose = TRUE, ...) 
{
  if (!class(range) %in% c("SpatialPolygonsDataFrame", "SpatialPolygons", 
                           "RasterLayer")) 
    stop("range must be a spatialPolygons* object or raster object")
  if (grepl("SpatialPolygons", class(range))) {
    urange = methods::as(gUnionCascaded(range), "SpatialPolygonsDataFrame")
    bc = coordinates(gCentroid(urange))
    uproj = projection(urange)
    bproj = paste0("+proj=eqc +lon_0=", bc[2])
    urange2 = spTransform(urange, bproj)
    if (!missing(points)) {
      points2 = spTransform(points, bproj)
      urange_outer = gUnion(urange2, gBuffer(points2, width = 0.001))
    }
    else urange_outer = urange2
    brange1 = gBuffer(urange_outer, width = domainkm * 1000)
    brange = spTransform(brange1, uproj)
    cdomain = crop(domain, brange)
    if (verbose) 
      writeLines("Rasterizing range to ROI")
    rrange = rasterize(methods::as(urange, "SpatialPolygons"), 
                       cdomain, field = -1, background = 1)
    lapply(list(cdomain), function(x) rmRaster(x))
    rbound = boundaries(rrange, classes = TRUE, asNA = T)
  }
  if (class(range) == "RasterLayer") {
    range_unique = unique(range)
    if (!all.equal(range_unique, c(0, 1))) 
      stop(paste("Range raster must include only 0s and 1s (and NAs for areas outside the domain),", 
                 "currently it contains the following values (may be truncated): ", 
                 paste(range_unique, collapse = ",")))
    rrange = reclassify(range, rcl = matrix(c(-0.1, 0.1, 
                                              1, 0.9, 1.1, -1), nrow = 2, byrow = T))
    rrange_forboundary = reclassify(range, rcl = matrix(c(-0.1, 
                                                          0.1, NA, 0.9, 1.1, 1), nrow = 2, byrow = T))
    rbound = boundaries(rrange_forboundary, classes = TRUE, 
                        asNA = T)
  }
  if (fact > 1) {
    rrange2 = aggregate(rrange, fact, max)
    rbound3 = aggregate(rbound, fact, max)
  }
  else {
    rrange2 = rrange
    rbound3 = rbound
  }
  if (verbose) 
    writeLines("Calculating distances")
  rdist1 = distance(rbound3)
  rdist2 = rrange2 * rdist1/1000
  if (verbose) 
    writeLines("Resampling back to original resolution")
  lapply(list(rrange, rbound, rrange2, rbound3, rdist1), function(x) rmRaster(x))
  if (!mask) 
    if (fact > 1) {
      return(resample(rdist2, rrange, ...))
    }
  else {
    return(rdist2)
  }
  if (mask) {
    if (fact > 1) {
      rdist3 = resample(rdist2, rrange)
    }
    else {
      rdist3 = rdist2
    }
    if (verbose) 
      writeLines("Masking to buffered range")
    rdist4 = mask(rdist3, brange, ...)
    rmRaster(rdist3)
    return(rdist4)
  }
}
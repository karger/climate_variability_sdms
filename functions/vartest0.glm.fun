# Written by : Niklaus E. Zimmermann, Ph.D.
#              Swiss Federal Research Institute WSL
#              Zuercherstrasse 111
#              CH-8903 Birmensdorf
#              nez@wsl.ch

# This function evaluates the predictive power of individual
# climate variables as available in sequence within a data frame

vartest0.glm <- function(spp,clmobj)
{
   ql.matrix<-matrix(ncol=1,nrow=dim(clmobj)[2])
   for (i in 1:(dim(clmobj)[2]))
   {
      tmp <- glm(spp>0 ~ clmobj[,i] + I((clmobj[,i])^2), na.action=na.omit, family=binomial)
      ql.matrix[(i),1] <- (1-(tmp$deviance/tmp$null.deviance))
   }
   return(ql.matrix)
}



#   ql.matrix<-matrix(ncol=2,nrow=13)
#   for (i in cf:(cf+12))
#   {
#      tmp <- glm(sppobj[,spi]>0 ~ clmobj[,i] + I((clmobj[,i])^2), na.action=na.omit, family=binomial)
#      ql.matrix[(i-cf+1),1] <- tmp$aic
#      ql.matrix[(i-cf+1),2] <- (1-(tmp$deviance/tmp$null.deviance))
#   }
#   return(ql.matrix)

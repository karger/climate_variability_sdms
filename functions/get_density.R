#' get density based on x y coordinates
#' 
#' @param x vector of x values
#' @param y vector of y values
#' @returns vector.
#' @examples
#' x <- sample(seq(1,100,1),20)
#' y <- sample(seq(1,100,1),20)
#' get_density(x, y)

get_density <- function(x, y, ...) {
  dens <- MASS::kde2d(x, y, ...)
  ix <- findInterval(x, dens$x)
  iy <- findInterval(y, dens$y)
  ii <- cbind(ix, iy)
  return(dens$z[ii])
}

